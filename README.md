# @mozardbv/mozard-alfredworkflow

Mozard workflow voor Alfred 5.

- [Meld een bevinding](https://intranet.mozard.nl/mozard/!suite09.scherm1089?mWfr=367)
- [Verzoek nieuwe functionaliteit](https://intranet.mozard.nl/mozard/!suite09.scherm1089?mWfr=604&mDdv=990842)

## Inhoudsopgave

- [@mozardbv/mozard-alfredworkflow](#mozardbvmozard-alfredworkflow)
  - [Inhoudsopgave](#inhoudsopgave)
  - [Over dit project](#over-dit-project)
    - [Gebouwd met](#gebouwd-met)
  - [Aan de slag](#aan-de-slag)
    - [Afhankelijkheden](#afhankelijkheden)
    - [Installatie](#installatie)
  - [Gebruik](#gebruik)
  - [Versioning](#versioning)
  - [Auteurs](#auteurs)
  - [Licentie](#licentie)

## Over dit project

Implementeert het zoeken in Mozard op basis van virtuele zoeksleutels in Alfred.

Dit is een persoonlijk hobbyproject en valt niet onder de servicenormen of verantwoordelijkheid van Mozard, tenzij expliciet anders overeengekomen.

### Gebouwd met

- [Alfred](https://www.alfredapp.com/)

## Aan de slag

### Afhankelijkheden

- Alfred >= 5.0.0

### Installatie

Download de meest recente versie van [Mozard.alfredworkflow](https://gitlab.com/MozardBV/mozard-alfredworkflow/-/raw/main/Mozard.alfredworkflow) en dubbelklik om in Alfred te installeren.

## Gebruik

Gebruik de volgende virtuele zoeksleutels in Alfred:

- z`...` voor zaken
- d`...` voor documenten
- m`...` voor mappen

## Versioning

Gebruikt [SemVer](https://semver.org/).

## Auteurs

- **Patrick Godschalk (Mozard)** - _Maintainer_ - [pgodschalk](https://gitlab.com/pgodschalk)

Zie ook de lijst van [contributors](https://gitlab.com/mozardbv/mozard-alfredworkflow/main) die hebben bijgedragen aan dit project.

## Licentie

[SPDX](https://spdx.org/licenses/) license: `UNLICENSED`

Copyright (c) 2006-2022 Mozard B.V.

[Leveringsvoorwaarden](https://www.mozard.nl/mozard/!suite86.scherm0325?mPag=204&mLok=1)
